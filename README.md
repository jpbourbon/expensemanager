Expense Manager API
===============

About
-----

This is a simple API for an Expense Management Module.

It offers a CRUD-like REST API for the Expense entity, which has a many-to-one relationship with the Expense Type entity. I.e., many expenses belong to only one expense type.

THis project is based on Laravel 8.

NOTE: body payload is expectod to be application/json.
 
 Install
 -------
 The development environment is containerized with a pre-built bitname image for the api and another for the mysql database.

 Please make sure docker is running in your system.

 After cloning the repository navigate to **expensemanage/api** and run
 
 ```
 docker-compose up -d
 ```

 This should successfully download the images, bring the server up and initialize the Laravel framework.

 **NOTE:** Migrations should be run automatically on the first build of th edocker images. However, the seeding of data is not done automatically. TO do this, runÇ
 ```
 docker exec -ti api_expensemanager_1 bash
 ```

 to get inside the api container, and then:
 ```
 composer db-refresh
 ```


 Running
 -------
 The server is served on **http://localhost:3000**.

 THe url for the api is **/api/expenses**.

 THe Expsense Type entity is seeded automatically from the list defined in **config/dbassets.php**.


 Tests
 -----
 Before running tests, please create a file for the sqlite database used for testing.
 
 ```
 touch api/my_database
 ```

 To run tests, please run

 ```
 docker exec -ti api_expensemanager_1 bash
 ```

 to get inside the api container, and then:
 
 ```
 composer test
 ```

 Documentation
 -------------
 Documentation is available on the **expensemanager/docs** folder.

 An OpenAPI/Swagger interface is provided at http://localhost:3000/api/documentation
