<?php

namespace Tests\Feature\Expense;

use Tests\fixtures\ExpenseInput;
use Tests\TestCase;

/**
 * @package Tests\Feature\Expense
 */
class ExpenseGetAllTest extends TestCase
{
    /**
     * Expenses can be listed
     *
     * @return void
     */
    public function test_can_get_all_expenses_200()
    {
        $data = [
            "description" => $description = "Foo",
            "value" => $value = 123.4,
            "type_id" => $typeId = 3,
        ];

        // Create a new entry to be returned
        $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

        $response = $this->get('/api/expenses');

        $response
            ->assertStatus(200)
            ->assertJson([
                "data" => true,
                "data" => [
                    0 => true,
                    0 => [
                        "data" => true,
                        "data" => [
                            "id" => true,
                            "id" => 1,
                            "description" => true,
                            "description" => $description,
                            "value" => true,
                            "value" => $value,
                            "type" => true,
                            "type" => [
                                "id" => true,
                                "id" => $typeId,
                                "name" => true,
                            ],
                        ],
                    ],
                ],
                "links" => true,
                "links" => [
                    "self" => true,
                ],
                "meta" => true,
                "meta" => [
                    "count" => true,
                    "count" => 1,
                ],
            ]);
    }
}
