<?php

namespace Tests\Feature\Expense;

use Tests\TestCase;
use Tests\fixtures\ExpenseInput;

class ExpenseGetOneTest extends TestCase
{
    /**
     * Expenses can be returned by Id
     *
     * @return void
     */
    public function test_can_get_one_expense_by_id_200()
    {
        $data = [
            "description" => $description = "Foo",
            "value" => $value = 123.4,
            "type_id" => $typeId = 3,
        ];

        $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

        $response = $this->get('/api/expenses/1');

        $response
            ->assertStatus(200)
            ->assertJson([
                "data" => true,
                "data" => [
                    "id" => true,
                    "id" => 1,
                    "description" => true,
                    "description" => $description,
                    "value" => true,
                    "value" => $value,
                    "type" => true,
                    "type" => [
                        "id" => true,
                        "id" => $typeId,
                        "name" => true,
                    ],
                ],
                "links" => true,
                "links" => [
                    "self" => true,
                ],
            ]);
    }

    /**
     * Expenses can't be found with an unexisting id
     *
     * @return void
     */
    public function test_returns_404_if_id_not_found()
    {
        $response = $this->get('/api/expenses/1');

        $response
            ->assertStatus(404)
            ->assertJson([
                "error" => true,
            ]);
    }

    /**
     * Expenses can't be found with an invalid id
     *
     * @return void
     */
    public function test_returns_422_if_id_not_valid()
    {
        $response = $this->get('/api/expenses/abc');

        $response
            ->assertStatus(422)
            ->assertJson([
                "error" => true,
            ]);
    }
}
