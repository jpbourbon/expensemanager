<?php

namespace Tests\Feature\Expense;

use Tests\TestCase;
use Tests\fixtures\ExpenseInput;

class ExpenseDeleteTest extends TestCase
{
    /**
     * Expenses can be deleted
     *
     * @return void
     */
    public function test_expense_can_be_deleted_200()
    {
        $data = [
            "description" => $description = "Foo",
            "value" => $value = 123.4,
            "type_id" => $typeId = 3,
        ];

        $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

        $response = $this->delete('/api/expenses/1', [], ExpenseInput::HEADERS);

        $response
            ->assertStatus(200);
    }

    /**
     * Expenses can't be found and deleted with an unexisting id
     *
     * @return void
     */
    public function test_returns_404_if_id_is_unknown()
    {
        $response = $this->delete('/api/expenses/1', [], ExpenseInput::HEADERS);

        $response
            ->assertStatus(404)
            ->assertJson([
                "error" => true,
            ]);
    }

    /**
     * Expenses can't be found and deleted with an invalid id
     *
     * @return void
     */
    public function test_returns_422_if_id_is_invalid()
    {
        $response = $this->delete('/api/expenses/abc', [], ExpenseInput::HEADERS);

        $response
            ->assertStatus(422)
            ->assertJson([
                "error" => true,
            ]);
    }
}
