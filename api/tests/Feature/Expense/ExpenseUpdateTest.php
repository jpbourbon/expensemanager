<?php

namespace Tests\Feature\Expense;

use Illuminate\Validation\Rules\In;
use Tests\TestCase;
use Tests\fixtures\ExpenseInput;

class ExpenseUpdateTest extends TestCase
{
    /**
     * An expense can be updated given the right parameters
     *
     * @return void
     */
    public function test_expense_can_be_updated_200()
    {
        $data = [
            "description" => "Foo",
            "value" => 123.4,
            "type_id" => 3,
        ];

        $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

        $updatedData = [
            "description" => $description = "Bar",
            "value" => $value = 1.4,
            "type_id" => $typeId = 4,
        ];

        $response = $this->put('/api/expenses/1', $updatedData, ExpenseInput::HEADERS);

        $response
            ->assertStatus(200)
            ->assertJson([
                "data" => true,
                "data" => [
                    "id" => true,
                    "id" => 1,
                    "description" => true,
                    "description" => $description,
                    "value" => true,
                    "value" => $value,
                    "type" => true,
                    "type" => [
                        "id" => true,
                        "id" => $typeId,
                        "name" => true,
                    ],
                ],
            ]);
    }

    /**
     * An expense won't pass validation without the right parameters and values
     *
     * @return void
     */
    public function test_returns_422_wuth_invalid_parameters()
    {
        $data = [
            "description" => "Foo",
            "value" => 123.4,
            "type_id" => 3,
        ];

        // Create a record
        $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

        $updatedDataArray = ExpenseInput::INVALID_PARAMS;

        foreach ($updatedDataArray as $updatedData) {
            $response = $this->put('/api/expenses/1', $updatedData, ExpenseInput::HEADERS);

            $response
                ->assertStatus(422)
                ->assertJson([
                    "error" => true,
                    "details" => true,
                ]);
        }
    }

    /**
     * An expense can't be updated if the expense id is not valid
     *
     * @return void
     */
    public function test_returns_422_if_id_not_valid()
    {
        $data = [
            "description" => "Foo",
            "value" => 123.4,
            "type_id" => 3,
        ];

        $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

        $updatedData = [
            "description" => "Bar",
            "value" => 123.4,
            "type_id" => 1,
        ];

        $response = $this->put('/api/expenses/abc', $updatedData, ExpenseInput::HEADERS);

        $response
            ->assertStatus(422)
            ->assertJson([
                "error" => true,
            ]);
    }

    /**
     * An expense can't be updated if the expense id does not exist
     *
     * @return void
     */
    public function test_returns_404_if_id_unknownl()
    {
        $data = [
            "description" => "Foo",
            "value" => 123.4,
            "type_id" => 3,
        ];

        $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

        $updatedData = [
            "description" => "Bar",
            "value" => 123.4,
            "type_id" => 1,
        ];

        $response = $this->put('/api/expenses/99999', $updatedData, ExpenseInput::HEADERS);

        $response
            ->assertStatus(404)
            ->assertJson([
                "error" => true,
            ]);
    }

    /**
     * An expense can't be updated if the expense type id does not exist
     *
     * @return void
     */
    public function test_returns_404_if_expense_type_id_unknowntest_update_expense_404_2_fail()
    {
        $data = [
            "description" => "Foo",
            "value" => 123.4,
            "type_id" => 3,
        ];

        $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

        $updatedData = [
            "description" => "Bar",
            "value" => 123.4,
            "type_id" => 999999,
        ];

        $response = $this->put('/api/expenses/1', $updatedData, ExpenseInput::HEADERS);

        $response
            ->assertStatus(404)
            ->assertJson([
                "error" => true,
            ]);
    }
}
