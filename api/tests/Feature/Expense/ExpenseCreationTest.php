<?php

namespace Tests\Feature\Expense;

use Tests\TestCase;
use Tests\fixtures\ExpenseInput;

class ExpenseCreationTest extends TestCase
{
    /**
     * An expense can be created given the right parameters
     *
     * @return void
     */
    public function test_expense_can_be_created_201_success()
    {
        $data = [
            "description" => $description = "Foo",
            "value" => $value = 123.4,
            "type_id" => $typeId = 3,
        ];

        $response = $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

        $response
            ->assertStatus(201)
            ->assertJson([
                "data" => true,
                "data" => [
                    "id" => true,
                    "id" => 1,
                    "description" => true,
                    "description" => $description,
                    "value" => true,
                    "value" => $value,
                    "type" => true,
                    "type" => [
                        "id" => true,
                        "id" => $typeId,
                        "name" => true,
                    ],
                ],
            ]);
    }

    /**
     * An expense won't pass validation without the right parameters and values
     *
     * @return void
     */
    public function  test_returns_422_if_parameters_are_invalid()
    {

        $dataArray = ExpenseInput::INVALID_PARAMS;

        foreach ($dataArray as $data) {
            $response = $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

            $response
                ->assertStatus(422)
                ->assertJson([
                    "error" => true,
                    "details" => true,
                ]);
        }
    }

    /**
     * An expense can't be created if the expense type id does not exist
     *
     * @return void
     */
    public function  test_returns_404_if_expense_type_id_unknown()
    {
        $data = [
            "description" => "foo",
            "value" => 123.4,
            "type_id" => 999999,
        ];

        $response = $this->post('/api/expenses', $data, ExpenseInput::HEADERS);

        $response
            ->assertStatus(404)
            ->assertJson([
                "error" => true,
            ]);
    }
}
