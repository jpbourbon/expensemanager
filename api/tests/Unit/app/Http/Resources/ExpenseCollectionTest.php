<?php

namespace Tests\Unit\app\Http\Resources;

use App\Http\Resources\ExpenseCollection;
use App\Http\Resources\ExpenseResource;
use App\Models\Expense;
use App\Models\Expense_Type;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Tests\TestCase;

class ExpenseCollectionTest extends TestCase
{
    public function test_toArray(): void
    {
        $request = new Request();
        $model = new Expense();
        $model->description = "foo";
        $model->value = 1.1;
        $model->type()->associate(new Expense_Type());

        $collection = new ExpenseCollection([$model]);
        $result = $collection->toArray($request);

        $this->assertEquals([
            "data" => new Collection([new ExpenseResource($model)]),
            "links" => [
                "self" => "http://:",
            ],
            "meta" => [
                "count" => 1,
            ],
        ], $result);
    }
}
