<?php

namespace Tests\Unit\app\Http\Resources;

use App\Http\Resources\ExpenseResource;
use App\Models\Expense;
use App\Models\Expense_Type;
use Illuminate\Http\Request;
use Tests\TestCase;

/**
 * 
 * @package Tests\Unit\app\Http\Resources
 */
class ExpenseResourceTest extends TestCase
{
    public function test_toArray(): void
    {
        $request = new Request();
        $model = new Expense();
        $model->description = "foo";
        $model->value = 1.1;
        $model->type()->associate(new Expense_Type());

        $resource = new ExpenseResource($model);
        $result = $resource->toArray($request);

        $this->assertEquals([
            "data" => [
                "id" => null,
                "description" => "foo",
                "value" => 1.1,
                "type" => new Expense_Type(),
            ],
            "links" => [
                "self" => "http://:",
            ],
        ], $result);
    }

    public function test_toArray_returns_link_to_specific_resources_with(): void
    {
        $request = new Request();
        $request->server->set('REQUEST_URI', '/foo/bar');
        $request->server->set('SERVER_NAME', 'localhost');
        $request->server->set('SERVER_PORT', 3000);

        $model = new Expense();
        $model->id = 1;
        $model->description = "foo";
        $model->value = 1.1;
        $model->type()->associate(new Expense_Type());

        $resource = new ExpenseResource($model);
        $methods = ["POST", "GET"];

        foreach ($methods as $method) {
            $request->setMethod($method);
            $result = $resource->toArray($request);

            $this->assertEquals("http://localhost:3000/foo/bar/1", $result["links"]["self"]);
        }
    }
}
