<?php

namespace Tests\Unit\app\Http\Controllers;

use App\Http\Controllers\Expense\ExpenseController;
use App\Http\Resources\ExpenseResource;
use App\Http\Validators\ExpenseValidator;
use App\Repositories\ExpenseRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Mockery\MockInterface;
use Tests\TestCase;
use Tests\fixtures\ExpenseInput;

class ExpenseControllerUpdateTest extends TestCase
{
    public function test_update(): void
    {
        $request = new Request();
        $arg1 = 1;
        $arg2 = [
            "description" => "ffoo",
            "value" => 1.23,
            "type_id" => 1,
        ];
        $request->merge($arg2);

        /** @var MockObject|ResponseInterface|ExpenseRepository */
        $mock = $this->mock(ExpenseRepository::class, function (MockInterface $mock) use ($arg1, $arg2) {
            $mock
                ->shouldReceive('update')
                ->with($arg1, $arg2)
                ->once()
                ->andReturn(new ExpenseResource([]));
        });

        $controller = new ExpenseController($mock, new ExpenseValidator());

        $result = $controller->update($request, $arg1);

        $this->assertInstanceOf(ExpenseResource::class, $result);
    }

    public function test_update_id_validation_exception(): void
    {
        $request = new Request();

        /** @var MockObject|ResponseInterface|ExpenseRepository */
        $mock = $this->mock(ExpenseRepository::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('update');
        });

        $controller = new ExpenseController($mock, new ExpenseValidator());

        $args = ExpenseInput::INVALID_ID_ARGUMENTS;

        foreach ($args as $arg) {
            // Tests several exceptions with a custom try/catch to provent execution stopping
            try {
                $controller->update($request, $arg);
                $this->assertFalse(True);
            } catch (ValidationException $e) {
                $this->assertTrue(true);
            }
        }
    }

    public function test_update_params_validation_exception(): void
    {
        /** @var MockObject|ResponseInterface|ExpenseRepository */
        $mock = $this->mock(ExpenseRepository::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('update');
        });

        $controller = new ExpenseController($mock, new ExpenseValidator());

        $parameters = ExpenseInput::INVALID_PARAMS;

        foreach ($parameters as $params) {
            $request = new Request();
            $request->merge($params);
            // Tests several exceptions with a custom try/catch to provent execution stopping
            try {
                $controller->update($request, 1);
                $this->assertFalse(True);
            } catch (ValidationException $e) {
                $this->assertTrue(true);
            }
        }
    }
}
