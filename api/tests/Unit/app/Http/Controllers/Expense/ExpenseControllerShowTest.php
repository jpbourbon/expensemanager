<?php

namespace Tests\Unit\app\Http\Controllers;

use App\Http\Controllers\Expense\ExpenseController;
use App\Http\Resources\ExpenseResource;
use App\Http\Validators\ExpenseValidator;
use App\Repositories\ExpenseRepository;
use Illuminate\Validation\ValidationException;
use Mockery\MockInterface;
use Tests\TestCase;
use Tests\fixtures\ExpenseInput;

class ExpenseControllerShowTest extends TestCase
{
    public function test_show(): void
    {
        $arg1 = 1;

        /** @var MockObject|ResponseInterface|ExpenseRepository */
        $mock = $this->mock(ExpenseRepository::class, function (MockInterface $mock) use ($arg1) {
            $mock
                ->shouldReceive('find')
                ->with($arg1)
                ->once()
                ->andReturn(new ExpenseResource([]));
        });

        $controller = new ExpenseController($mock, new ExpenseValidator());

        $result = $controller->show($arg1);

        $this->assertInstanceOf(ExpenseResource::class, $result);
    }

    public function test_show_id_validation_exception(): void
    {
        /** @var MockObject|ResponseInterface|ExpenseRepository */
        $mock = $this->mock(ExpenseRepository::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('find');
        });

        $controller = new ExpenseController($mock, new ExpenseValidator());

        $args = ExpenseInput::INVALID_ID_ARGUMENTS;

        foreach ($args as $arg) {
            // Tests several exceptions with a custom try/catch to provent execution stopping
            try {
                $controller->show($arg);
                $this->assertFalse(True);
            } catch (ValidationException $e) {
                $this->assertTrue(true);
            }
        }
    }
}
