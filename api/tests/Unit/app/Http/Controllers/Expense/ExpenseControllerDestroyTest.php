<?php

namespace Tests\Unit\app\Http\Controllers;

use App\Http\Controllers\Expense\ExpenseController;
use App\Http\Validators\ExpenseValidator;
use App\Repositories\ExpenseRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Mockery\MockInterface;
use Tests\TestCase;
use Tests\fixtures\ExpenseInput;

class ExpenseControllerDestroyTest extends TestCase
{
    public function test_destroy(): void
    {
        $arg1 = 1;

        /** @var MockObject|ResponseInterface|ExpenseRepository */
        $mock = $this->mock(ExpenseRepository::class, function (MockInterface $mock) use ($arg1) {
            $mock
                ->shouldReceive('delete')
                ->with($arg1)
                ->once()
                ->andReturn(true);
        });

        $controller = new ExpenseController($mock, new ExpenseValidator());

        $result = $controller->destroy($arg1);

        $this->assertInstanceOf(JsonResponse::class, $result);
    }

    public function test_delete_id_validation_exception(): void
    {
        /** @var MockObject|ResponseInterface|ExpenseRepository */
        $mock = $this->mock(ExpenseRepository::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('delete');
        });

        $controller = new ExpenseController($mock, new ExpenseValidator());

        $args = ExpenseInput::INVALID_ID_ARGUMENTS;

        foreach ($args as $arg) {
            // Tests several exceptions with a custom try/catch to provent execution stopping
            try {
                $controller->destroy($arg);
                $this->assertFalse(True);
            } catch (ValidationException $e) {
                $this->assertTrue(true);
            }
        }
    }
}
