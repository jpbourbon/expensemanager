<?php

namespace Tests\Unit\app\Http\Controllers;

use App\Http\Controllers\Expense\ExpenseController;
use App\Http\Resources\ExpenseResource;
use App\Http\Validators\ExpenseValidator;
use App\Repositories\ExpenseRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Mockery\MockInterface;
use Tests\TestCase;
use Tests\fixtures\ExpenseInput;

class ExpenseControllerStoreTest extends TestCase
{
    public function test_store(): void
    {
        $request = new Request();
        $arg1 = [
            "description" => "ffoo",
            "value" => 1.23,
            "type_id" => 1,
        ];
        $request->merge($arg1);

        /** @var MockObject|ResponseInterface|ExpenseRepository */
        $mock = $this->mock(ExpenseRepository::class, function (MockInterface $mock) use ($arg1) {
            $mock
                ->shouldReceive('create')
                ->with($arg1)
                ->once()
                ->andReturn(new ExpenseResource([]));
        });

        $controller = new ExpenseController($mock, new ExpenseValidator());

        $result = $controller->store($request);

        $this->assertInstanceOf(ExpenseResource::class, $result);
    }

    public function test_store_params_validation_exception(): void
    {
        /** @var MockObject|ResponseInterface|ExpenseRepository */
        $mock = $this->mock(ExpenseRepository::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('create');
        });

        $controller = new ExpenseController($mock, new ExpenseValidator());

        $parameters = ExpenseInput::INVALID_PARAMS;

        foreach ($parameters as $params) {
            $request = new Request();
            $request->merge($params);
            // Tests several exceptions with a custom try/catch to provent execution stopping
            try {
                $controller->store($request);
                $this->assertFalse(True);
            } catch (ValidationException $e) {
                $this->assertTrue(true);
            }
        }
    }
}
