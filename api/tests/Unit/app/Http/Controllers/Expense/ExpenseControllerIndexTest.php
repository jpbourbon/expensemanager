<?php

namespace Tests\Unit\app\Http\Controllers;

use App\Http\Validators\ExpenseValidator;
use App\Http\Controllers\Expense\ExpenseController;
use App\Http\Resources\ExpenseCollection;
use App\Repositories\ExpenseRepository;
use Mockery\CountValidator\Exact;
use Mockery\MockInterface;
use Tests\TestCase;

class ExpenseControllerIndexTest extends TestCase
{
    public function test_index(): void
    {
        /** @var MockObject|ResponseInterface|ExpenseRepository */
        $mock = $this->mock(ExpenseRepository::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('all')
                ->once()
                ->andReturn(new ExpenseCollection([]));
        });

        $controller = new ExpenseController($mock, new ExpenseValidator());

        $result = $controller->index();

        $this->assertInstanceOf(ExpenseCollection::class, $result);
    }
}
