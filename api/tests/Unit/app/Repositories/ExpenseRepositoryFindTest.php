<?php

namespace tests\Unit\app\Repositories;

use App\Http\Resources\ExpenseResource;
use App\Models\Expense;
use App\Models\Expense_Type;
use App\Repositories\ExpenseRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mockery\MockInterface;
use Tests\TestCase;

class ExpenseRepositoryFindTest extends TestCase
{
    public function test_can_get_one(): void
    {
        /** @var MockObject|ResponseInterface|Expense */
        $mock = $this->mock(Expense::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('findOrFail')
                ->with(1)
                ->once()
                ->andReturn(new Expense());
        });

        $repository = new ExpenseRepository($mock, new Expense_Type());

        $result = $repository->find(1);

        $this->assertInstanceOf(ExpenseResource::class, $result);
    }

    public function test_throws_exception_on_id_not_found(): void
    {
        /** @var MockObject|ResponseInterface|Expense */
        $mock = $this->mock(Expense::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('findOrFail')
                ->with(1)
                ->once()
                ->andThrow(new ModelNotFoundException());
        });

        $repository = new ExpenseRepository($mock, new Expense_Type());

        $this->expectException(ModelNotFoundException::class);

        $repository->find(1);
    }
}
