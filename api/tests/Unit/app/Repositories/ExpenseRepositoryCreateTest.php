<?php

namespace tests\Unit\app\Repositories;

use App\Exceptions\DBException;
use App\Http\Resources\ExpenseResource;
use App\Models\Expense;
use App\Models\Expense_Type;
use App\Repositories\ExpenseRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mockery\MockInterface;
use Tests\TestCase;

class ExpenseRepositoryCreateTest extends TestCase
{
    public function test_can_create(): void
    {
        $arg1 = 1;
        $arg2 = [
            "description" => "foo",
            "value" => "12.3",
            "type_id" => $arg1,
        ];

        /** @var MockObject|ResponseInterface|Expense_Type */
        $mockET = $this->mock(Expense_Type::class, function (MockInterface $mockET) use ($arg1) {
            $mockET
                ->shouldReceive('findOrFail')
                ->with($arg1)
                ->once()
                ->andReturn($mockET);
        })->makePartial();

        /** @var MockObject|ResponseInterface|Expense */
        $mock = $this->mock(Expense::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('save')
                ->once()
                ->andReturn(true);
        })->makePartial();



        $repository = new ExpenseRepository($mock, $mockET);

        $result = $repository->create($arg2);

        $this->assertInstanceOf(ExpenseResource::class, $result);
    }

    public function test_throws_exception_when_foreign_key_not_found(): void
    {
        $arg1 = 1;
        $arg2 = [
            "description" => "foo",
            "value" => "12.3",
            "type_id" => $arg1,
        ];

        /** @var MockObject|ResponseInterface|Expense_Type */
        $mockET = $this->mock(Expense_Type::class, function (MockInterface $mockET) use ($arg1) {
            $mockET
                ->shouldReceive('findOrFail')
                ->with($arg1)
                ->once()
                ->andThrow(new ModelNotFoundException());
        })->makePartial();

        $repository = new ExpenseRepository(new Expense(), $mockET);

        $this->expectException(ModelNotFoundException::class);

        $repository->create($arg2);
    }

    public function test_throws_exception_on_db_write_error(): void
    {
        $arg1 = 1;
        $arg2 = [
            "description" => "foo",
            "value" => "12.3",
            "type_id" => $arg1,
        ];

        /** @var MockObject|ResponseInterface|Expense_Type */
        $mockET = $this->mock(Expense_Type::class, function (MockInterface $mockET) use ($arg1) {
            $mockET
                ->shouldReceive('findOrFail')
                ->with($arg1)
                ->once()
                ->andReturn($mockET);
        })->makePartial();

        /** @var MockObject|ResponseInterface|Expense */
        $mock = $this->mock(Expense::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('save')
                ->once()
                ->andReturn(false);
        })->makePartial();

        $repository = new ExpenseRepository($mock, $mockET);

        $this->expectException(DBException::class);

        $repository->create($arg2);
    }
}
