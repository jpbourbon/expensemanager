<?php

namespace tests\Unit\app\Repositories;

use App\Http\Resources\ExpenseCollection;
use App\Models\Expense;
use App\Models\Expense_Type;
use App\Repositories\ExpenseRepository;
use Illuminate\Database\Eloquent\Collection;
use Mockery\MockInterface;
use Tests\TestCase;

class ExpenseRepositoryAllTest extends TestCase
{
    public function test_can_get_all(): void
    {
        /** @var MockObject|ResponseInterface|Expense */
        $mock = $this->mock(Expense::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('all')
                ->once()
                ->andReturn(new Collection([]));
        });

        $repository = new ExpenseRepository($mock, new Expense_Type());

        $result = $repository->all();

        $this->assertInstanceOf(ExpenseCollection::class, $result);
    }
}
