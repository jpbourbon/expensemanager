<?php

namespace tests\Unit\app\Repositories;

use App\Exceptions\DBException;
use App\Models\Expense;
use App\Models\Expense_Type;
use App\Repositories\ExpenseRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mockery\MockInterface;
use Tests\TestCase;

class ExpenseRepositoryDeleteTest extends TestCase
{
    public function test_can_delete(): void
    {
        /** @var MockObject|ResponseInterface|Expense */
        $mock = $this->mock(Expense::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('delete')
                ->once()
                ->andReturn(true);
            $mock
                ->shouldReceive('findOrFail')
                ->with(1)
                ->once()
                ->andReturn($mock);
        });

        $repository = new ExpenseRepository($mock, new Expense_Type());

        $result = $repository->delete(1);

        $this->assertEquals(null, $result);
    }

    public function test_throws_exception_on_id_not_found(): void
    {
        /** @var MockObject|ResponseInterface|Expense */
        $mock = $this->mock(Expense::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('findOrFail')
                ->with(1)
                ->once()
                ->andThrow(new ModelNotFoundException());
        });

        $repository = new ExpenseRepository($mock, new Expense_Type());

        $this->expectException(ModelNotFoundException::class);

        $repository->find(1);
    }

    public function test_throws_exception_on_db_write_error(): void
    {
        /** @var MockObject|ResponseInterface|Expense */
        $mock = $this->mock(Expense::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('delete')
                ->once()
                ->andReturn(false);
            $mock
                ->shouldReceive('findOrFail')
                ->with(1)
                ->once()
                ->andReturn($mock);
        });

        $repository = new ExpenseRepository($mock, new Expense_Type());

        $this->expectException(DBException::class);

        $repository->delete(1);
    }
}
