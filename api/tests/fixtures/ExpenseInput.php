<?php

namespace Tests\fixtures;

class ExpenseInput
{
    public const INVALID_ID_ARGUMENTS = ["abc", false, 1.1, [], "º", ""];

    public const INVALID_PARAMS = [
        [],
        ["description" => ""],
        ["description" => "foo"],
        ["description" => "foo"],
        ["description" => "foo", "value" => ""],
        ["description" => "foo", "value" => "abc"],
        ["value" => 1],
        ["description" => "foo", "value" => 1],
        ["description" => "foo", "value" => "abc", "type_id" => ""],
        ["foo" => "bar"],
    ];

    public const HEADERS = ["Accept" => "application/json"];
}
