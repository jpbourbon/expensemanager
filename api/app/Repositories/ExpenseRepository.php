<?php

namespace App\Repositories;

use App\Http\Resources\ExpenseCollection;
use App\Http\Resources\ExpenseResource;
use App\Exceptions\DBException;
use App\Models\Expense;
use App\Models\Expense_Type;

class ExpenseRepository implements IRepository
{
    protected $expenseModel;
    protected $expenseTypeModel;

    public function __construct(Expense $expense, Expense_Type $expenseType)
    {
        $this->expenseModel = $expense;
        $this->expenseTypeModel = $expenseType;
    }

    public function all(): ExpenseCollection
    {
        $collection = $this->expenseModel::all();

        return new ExpenseCollection($collection);
    }

    public function find(int $id): ExpenseResource
    {
        $resource = $this->expenseModel::findOrFail($id);

        return new ExpenseResource($resource);
    }

    public function create(array $data): ExpenseResource
    {
        extract($data);

        $resource = $this->expenseModel;

        $resource->description = $description;
        $resource->value = $value;

        $expenseType = $this->expenseTypeModel::findOrFail($type_id);

        $resource->type()->associate($expenseType);

        if (!$resource->save()) {
            throw new DBException();
        }

        return new ExpenseResource($resource);
    }

    public function update(int $id, array $data): ExpenseResource
    {
        extract($data);

        $resource = $this->expenseModel::findOrFail($id);

        $resource->description = $description;
        $resource->value = $value;

        $expenseType = $this->expenseTypeModel::findOrFail($type_id);

        $resource->type()->associate($expenseType);

        if (!$resource->save()) {
            throw new DBException();
        }

        return new ExpenseResource($resource);
    }

    public function delete(int $id): void
    {
        $expense = $this->expenseModel::findOrFail($id);

        if (!$expense->delete()) {
            throw new DBException();
        }
    }
}
