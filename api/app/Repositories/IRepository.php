<?php
namespace App\Repositories;

use App\Http\Resources\ExpenseCollection;
use App\Http\Resources\ExpenseResource;


interface IRepository
{
    public function all(): ExpenseCollection;
    public function find(int $id): ExpenseResource;
    public function create(array $data): ExpenseResource;
    public function update(int $id, array $data): ExpenseResource;
    public function delete(int $id): void;
}