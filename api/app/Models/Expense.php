<?php

namespace App\Models;

use App\Models\Expense_Type;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use HasFactory;

    public function type()
    {
        return $this->belongsTo(Expense_Type::class);
    }
}
