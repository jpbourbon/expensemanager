<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExpenseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // Adds id to resource url if requesting all or create
        $extra = (last($request->segments()) != $this->id
            && in_array($request->method(), ["GET", "POST"])
            ? "/{$this->id}"
            : ""
        );

        return [
            "data" => [
                "id" => $this->id,
                "description" => $this->description,
                "value" => (float) $this->value,
                "type" => $this->type,
            ],
            "links" => [
                "self" => $request->fullUrl() . $extra,
            ]
        ];
    }
}
