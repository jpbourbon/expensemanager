<?php

namespace App\Http\Validators;

use App\Http\Validators\ValidatorAbstract;
/**
 * 
 * @package App\Http\Validators
 */
class ExpenseValidator extends ValidatorAbstract
{
    /**
     * 
     * @var string[][]
     */
    protected $paramRules = [
        "description" => ["required", "string"],
        "value" => ["required", "numeric"],
        "type_id" => ["required", "integer"],
    ];
    /**
     * 
     * @var string[][]
     */
    protected $idRules = ["id" => ["required", "integer"]];

}
