<?php

namespace App\Http\Controllers\Expense;

use App\Http\Controllers\Controller;
use App\Http\Validators\ExpenseValidator;
use App\Repositories\ExpenseRepository;
use Illuminate\Http\Request;

/**
 * @OA\Info(title="Expense Manager API", version="0.1")
 */

class ExpenseController extends Controller
{
    private $repository;
    private $validator;

    public function __construct(ExpenseRepository $repository, ExpenseValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @OA\Get(
     *     path="/api/expenses",
     *     description="Display all the expenses",
     *     tags={"expense_manager"},
     *     operationId="lgetAllExpenses",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(),
     *         ),
     *     )
     * )
     */
    public function index()
    {
        return $this->repository->all();
    }

    /**
     * @OA\Post(
     *     path="/api/expenses",
     *     description="Add a new expense to the database",
     *     tags={"expense_manager"},
     *     operationId="storeExpense",
     *     @OA\Parameter(
     *         name="description",
     *         in="query",
     *         description="Expense description",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="value",
     *         in="query",
     *         description="Expense value",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="type_id",
     *         in="query",
     *         description="Expense Type identifier value",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="created",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Expense Type not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Invalid input"
     *     )
     * )
     */
    public function store(Request $request)
    {
        $this->validator->params($request);

        return $this->repository->create($request->all());
    }

    /**
     * @OA\Get(
     *     path="/api/expenses/{id}",
     *     tags={"expense_manager"},
     *     description="Finds Expenses by id",
     *     operationId="showExpense",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Identifier value",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Expense not found"
     *     )
     * )
     */
    public function show($id)
    {
        $this->validator->id($id);

        return $this->repository->find($id);
    }

    /**
     * @OA\Put(
     *     path="/api/expenses/{id}",
     *     description="Update an existing expense",
     *     tags={"expense_manager"},
     *     operationId="updateExpense",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Identifier value",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="description",
     *         in="query",
     *         description="Expense description",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="value",
     *         in="query",
     *         description="Expense value",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="type_id",
     *         in="query",
     *         description="Expense Type identifier value",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Expense not found / Expense Type not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Validation exception"
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        $this->validator
        ->id($id)
        ->params($request);

        return $this->repository->update($id, $request->all());
    }

    /**
     * @OA\Delete(
     *     path="/api/expenses/{id}",
     *     description="Deletes and existing expense",
     *     tags={"expense_manager"},
     *     operationId="deleteExpense",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Identifier value",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             type="string",
     *         ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Expense not found"
     *     )s
     * )
     */
    public function destroy($id)
    {
        $this->validator->id($id);

        $this->repository->delete($id);

        return response()->json("OK", 200);
    }
}
