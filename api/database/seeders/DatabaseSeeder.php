<?php

namespace Database\Seeders;

use App\Models\Expense_Type;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach (config("dbassets.expense_types") as $item) {
            $expenseType = new Expense_Type(["name" => $item]);
            $expenseType->save();
        }
    }
}
